# SyncProof
It is a network application that measures the latency of a network on TCP or UDP communication. The tool also reports clock drifts between the communicating nodes. The tool is initially developed to observe the clock errors once nodes are syncronized according to Precision Time Protocol (PTP).

## usage: syncproof [options] [argument]
### options
	-c [mac|ip]         client mode
	-s [mac|ip]         server mode
	-m                  server mode is in 'bounce', should be used on both sides. Default, 'trade' mode (OPTIONAL)
	-d                  interface/device name, e.g., eth0
	-S [frame size]     payload size, default= 1514 (OPTIONAL)
	-e                  use ETHERNET frames, default 1514 (OPTIONAL)
	-u                  use UDP packets
	-t                  use TCP packets
	-N                  number of frames to be used for test at a time. Max= 1000
	-i [interval]       interval between frames/packets in microseconds, default= 1000 microsecond (OPTIONAL)
	-v                  verbose or dump latency/sync error to the console
	-f [filename]       dump latency/sync error to a file
	-t [timeout]        inter-arrival of frame receive timeout in seconds
	-D        			full debug

For example, a server (192.168.250.226) listening to UDP communication via enp0s3 interface with frame size 100:
	
	$ sudo ./syncproof -s 192.168.250.226 -N 100 -u -d enp0s3
	enp0s3 interface index: 2
	mode: 0 server mode.
	server mode: trade ... started

, and a client communicating to the server on UDP, reports the following latency:
	
	$ sudo ./syncproof -c -s 192.168.250.226 -N 100 -u -d enp0s3
	enp0s3 interface index: 2
	mode: client mode.

	Thread: sender started ...

	Thread: receive_frame started ...

	latency = 0.000027418 error: = 0.000005245
	latency = 0.000022531 error: = -0.000000715
	latency = 0.000030518 error: = -0.000007629
	latency = 0.000069737 error: = -0.000035286
	maximum frames sent reached: 100
	latency = 0.000076413 error: = -0.000044584
	receive_frame(): timeout
	sent frames = 100 received frames = 100 confirmed frames = 100
	L 19.907951355 2383.708953857 125.261545181 304.841279132
	E 0.476837158 2271.175384521 69.034099579 293.214373344

### Contact: nesredin.mahmud@gmail.com
