/*
 * syncproof.h
 *
 *  Created on: Nov 13, 2013
 *      Author: Nesredin M.J <nesredin.mom@gmail.com>
 */

#ifndef SYNCPROOF_H_
#define SYNCPROOF_H_


#define handle_error_en(en, msg) \
               do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define BILLION 				1000000000L
#define	NR_OF_FRAMES			1000000
#define MSEC					1000000L
#define INTERVAL				1000

#define	CLIENT_MODE				0
#define	SERVER_MODE				1
#define	PROTO_ETHERNET			0
#define	PROTO_UDP				1
#define	PROTO_TCP				2
#define SERVER_TRADE_MODE		0
#define	SERVER_BOUNCE_MODE		1
#define VERBOSE_MODE			0
#define DUMP_TO_FILE_MODE		1

/* Signal information structure */
typedef struct signal_struct
{
	int	code;
	char name[20];
	unsigned int	seq;
	struct timespec sender_T1;
	struct timespec receiver_T2;
	struct timespec receiver_T3;
	struct timespec sender_T4;
	char message[128];
}Signal;

/* Argument structure to be passed to the 'send_frame()' funtion */
typedef struct argument_struct
{
	char iface_name[20];
	int ifindex;
	struct ifreq ifr;
	char mac_address[20];
	char message[128];
}Argument;

/* net measurement structure */
typedef struct net_stat_struct
{
	struct timespec sender_ts[NR_OF_FRAMES];
	struct timespec receiver_ts[NR_OF_FRAMES];
	struct timespec latency[NR_OF_FRAMES];
	int avg;
	int std;
	int min;
	int max;
}NetStat;

/* Sync calculation structure */
typedef struct sync_struct
{
	int	seq;
	struct timespec sender_T1;
	struct timespec receiver_T2;
	struct timespec receiver_T3;
	struct timespec sender_T4;
	int remark;
}SyncStat;

/* usage */
void usage();
inline double ts_to_sec(struct timespec *ts);
/* check mac address */
inline int compareMac(const unsigned char *mac1, const unsigned char * mac2);
/* minimum function */
double minFunc(double *arr, int length);
/* maximum function */
double maxFunc(double *arr, int length);
/* average function */
double avgFunc(double *arr, int length);
/* standard deviation function */
double stdFunc(double data[], int n);
/* print net stat */
inline void printNetStat();
/* signal handler */
void sig_handler();
/* Print signal information */
void printSignal(Signal *signal);
/* Print Ethernet Header */
void printEtherHeader(const void *buffer);
void *print_message_function( void *ptr);
inline void copy_timespec(struct timespec *destination, struct timespec *source);
inline void flush_signal(SyncStat *sync);
/* Send frame */
void *send_frame(void *ptr);
inline void calc_sync(SyncStat *sync_stat);
/* receive frame */
void *receive_frame(void *ptr);
#endif /* SYNCPROOF_H_ */
