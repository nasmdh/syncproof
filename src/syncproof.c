/*
 * Copyright (C) This is ABB copy right notice.
 *
 * Author: nesredin
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the PG_ORGANIZATION nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY	THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS-IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <time.h>
#include <signal.h>
#include <math.h>
#include <errno.h>
#include <ctype.h>

#include <sched.h>
#include <pthread.h>
#include <semaphore.h>

#include<string.h>
#include<sys/socket.h>
#include<netinet/in.h>

// Raw ethernet
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>

#include "syncproof.h"

NetStat latency_netstat;
int count = 0;
int bounced_frames = 0;
double latency[NR_OF_FRAMES];
double error[NR_OF_FRAMES];
int last_signal_seq = 0;		// This will tell us if the client is sending frames too fast as compared to the latency between
								// the client and server machines.
int received_frames = 0;		// The number of frames sent from client to the server
int sent_frames = 0;			// The number of frames received from server to client
								// If the values of received_frames and sent_frames at the time of receive_frames() is not the same
								// either the clinet is too fast sending frames or some unknown error has happended.
								// If the value of received_frames is not the same as the received fram seq number at receive_frames(),
								// unordered frames is received by the client which highly unprobably to happen.
int confirmed_frames = 0;		// Number of frames received successfully as compared to the frame sent from the client.
sem_t sem;
int *frame_mark; //[NR_OF_FRAMES];
int s; // socket
unsigned char src_mac[ETH_ALEN];
struct sockaddr_ll socket_address;				// target address

/* command line related variables */
int mode;
int frame_size = ETH_FRAME_LEN;
int	interval = 1000; // milliseconds
int number_of_frames = INTERVAL;
int socket_type = PROTO_ETHERNET;
int macx[ETH_ALEN];
unsigned char mac[ETH_ALEN];
char device[20];
SyncStat *sync_stat;//[NR_OF_FRAMES];
int server_mode = SERVER_TRADE_MODE;
int debug_mode = VERBOSE_MODE;
char debug_filename[20];
int debug_enable = 0;

/* usage */
void usage()
{
	puts("usage: syncproof [options] [argument]");
	puts("options");
	puts("\t-c [mac|ip]         client mode");
	puts("\t-s [mac|ip]         server mode");
	puts("\t-m                  server mode is in 'bounce', should be used on both sides. Default, 'trade' mode (OPTIONAL)");
	puts("\t-d                  interface/device name, e.g., eth0");
	puts("\t-S [frame size]     payload size, default= 1514 (OPTIONAL)");
	puts("\t-e                  use ETHERNET frames, default 1514 (OPTIONAL)");
	puts("\t-u                  use UDP packets");
	puts("\t-t                  use TCP packets");
	puts("\t-N                  number of frames to be used for test at a time. Max= 1000");
	puts("\t-i [interval]       interval between frames/packets in microseconds, default= 1000 microsecond (OPTIONAL)");
	puts("\t-v                  verbose or dump latency/sync error to the console");
	puts("\t-f [filename]       dump latency/sync error to a file");
	puts("\t-t [timeout]        inter-arrival of frame receive timeout in seconds");
	puts("\t-D        			full debug");
}

/* Process command line */
void proces_cmdline(int argc, char **argv)
{
	int c;
	int opterr = 0;

	while ((c = getopt (argc, argv, "c:s:S:euti:d:mvf:N:D")) != -1)
	{
		switch (c)
		{
		case 'S': // size of frame
			frame_size = atoi(optarg);
			break;
		case 'e': //type of socket protocol ETHERNET
			socket_type = PROTO_ETHERNET;
			break;
		case 'u': // type of socket protocol UDP
			socket_type = PROTO_UDP;
			break;
		case 't': // type of socket protocol TCP
			socket_type = PROTO_TCP;
			break;
		case 'd': // interface name to send or receive a frame
			strcpy(device, optarg);
			break;
		case 'c': // client mode
			mode = CLIENT_MODE; //stcpy(destination_address, optarg);
			if(socket_type == PROTO_ETHERNET)
			{
				sscanf(optarg, "%x:%x:%x:%x:%x:%x", &macx[0], &macx[1], &macx[2], &macx[3], &macx[4], &macx[5]);
				mac[0] = macx[0]; mac[1] = macx[1];
				mac[2] = macx[2]; mac[3] = macx[3];
				mac[4] = macx[4]; mac[5] = macx[5];
			}else if (socket_type == PROTO_UDP || socket_type == PROTO_TCP)
			{
				puts("TODO: UDP/TCP raw socket"); // inet_aton
			}
			break;
		case 's': // server mode
			mode = SERVER_MODE;
			if(socket_type == PROTO_ETHERNET)
			{
				sscanf(optarg, "%x:%x:%x:%x:%x:%x", &macx[0], &macx[1], &macx[2], &macx[3], &macx[4], &macx[5]);
				mac[0] = macx[0]; mac[1] = macx[1];
				mac[2] = macx[2]; mac[3] = macx[3];
				mac[4] = macx[4]; mac[5] = macx[5];
			}else if (socket_type == PROTO_UDP || socket_type == PROTO_TCP)
			{
				puts("TODO: UDP/TCP raw socket"); // inet_aton
			}
			break;
		case 'm': // server mode as bouncing (reflects back to the client), a frame or
					//trading a frame (a ready made frame is sent back to the client)
			server_mode = SERVER_BOUNCE_MODE;
			break;
		case 'i': // Inte arrival time between a sequence of frames
			interval = atoi(optarg);
			interval = (interval > 10)?(interval):(10);// make sure inter arrival of frames is not less than 10ms
														// otherwise, the system might consume much of the cpu which leads to
														// system crash due to the high priority of the application
			break;
		case 'N': // Number of frames
			number_of_frames = atoi(optarg);
			if (number_of_frames > NR_OF_FRAMES) { printf("Error: max. number of rames, N = %d\n", NR_OF_FRAMES); exit(EXIT_FAILURE);}
			break;
		case 'v': // dump summary of latency and sync error to console
			debug_mode = VERBOSE_MODE;
			break;
		case 'f': // dump summary of latency and sync error to a file
			debug_mode = DUMP_TO_FILE_MODE;
			strcpy(debug_filename, optarg);
			break;
		case 'D': // full debug client send, server receive, server send and client receive
			debug_enable = 1;
			break;
		case '?':
			if (optopt == 'S' || optopt == 'i')
			{
			   fprintf (stderr, "Option -%c requires an argument.\n", optopt);
			}else if (isprint(optopt))
			{
			   fprintf (stderr, "Unknown option `-%c'.\n", optopt);
			}else
			{
			   fprintf (stderr, "Unknown option character `\\x%x'.\n",optopt);
			}
			usage();
			break;
		default:
			usage(); break;
		}
	}
}

/* Convert timespec into second */
inline double ts_to_sec(struct timespec *ts)
{
	return (double)ts->tv_sec + (double)ts->tv_nsec/BILLION;
}

/* check mac address */
inline int compareMac(const unsigned char *mac1, const unsigned char * mac2)
{
	// check source address
	int i;
	for (i = 0; i < ETH_ALEN; ++i)
	{
		if (mac1[i] != mac2[i])
		{
			return 0;
		}
	}

	return 1;
}

/* minimum function */
double minFunc(double *arr, int length)
{
	double min = arr[0];
	int i;

	for (i = 1; i < length; ++i)
	{
		if (arr[i] < min)
		{
			min = arr[i];
		}
	}

	return min;
}

/* maximum function */
double maxFunc(double *arr, int length)
{
	double max = arr[0];

	int i;

	for (i = 1; i < length; ++i)
	{
		if (arr[i] > max)
		{
			max = arr[i];
		}
	}

	return max;
}

/* average function */
double avgFunc(double *arr, int length)
{
	int i;
	double sum = 0.0;

	for(i = 0; i < length; ++i)
	{
		sum += arr[i];
	}

	return (sum/length);
}

/* standard deviation function */
double stdFunc(double data[], int n)
{
	double mean=0.0, sum_deviation=0.0;
	int i;

	for(i=0; i<n;++i) { mean+=data[i]; }
	mean=mean/n;
	for(i=0; i<n;++i)
		sum_deviation+=(data[i]-mean)*(data[i]-mean);

	return sqrt(sum_deviation/n);
}


/* Calculate latency and error of two machines clock */
inline void calc_sync(SyncStat *sync_stat)
{
	double T1, T2, T3, T4;
	//double latency, error;

	if (sync_stat->remark == 2)
	{
		T1 = (double)sync_stat->sender_T1.tv_sec + ((double)sync_stat->sender_T1.tv_nsec/BILLION);
		T2 = (double)sync_stat->receiver_T2.tv_sec + ((double)sync_stat->receiver_T2.tv_nsec/BILLION);
		T3 = (double)sync_stat->receiver_T3.tv_sec + ((double)sync_stat->receiver_T3.tv_nsec/BILLION);
		T4 = (double)sync_stat->sender_T4.tv_sec + ((double)sync_stat->sender_T4.tv_nsec/BILLION);

		//printf("%f\n%f\n%f\n%f\n", (double)sync_stat->sender_T1.tv_nsec,(double)sync_stat->receiver_T2.tv_nsec, (double)sync_stat->receiver_T3.tv_nsec, (double)sync_stat->sender_T4.tv_nsec);
		//printf("T1 = %.9f T2 = %.9f T3 = %.9f T4 = %.9f\n", T1, T2, T3, T4);
		if (server_mode == SERVER_BOUNCE_MODE)
		{
			latency[confirmed_frames] = ((T2 - T1) + (T4 - T3))/2;
			error[confirmed_frames] = ((T4 - T3) - (T2-T1))/2;
			++confirmed_frames;
		}
		else if(server_mode == SERVER_TRADE_MODE)
		{
			latency[confirmed_frames] = (T4 - T1)/2;
			error[confirmed_frames] = (T4 + T1-2*T2)/2;
			printf("latency = %.9f error: = %.9f\n", latency[confirmed_frames], error[confirmed_frames]);
			++confirmed_frames;
		}

	}else
	{
		printf("WARNING: calc_sync(): corrupt entry in sync table remark = %d\n", sync_stat->remark);
		exit(1);
	}
}

/* print net stat */
inline void printNetStat()
{
	int i;
	FILE *ofile = NULL;

	// summary of number of frames
	printf("sent frames = %d received frames = %d confirmed frames = %d\n", sent_frames, received_frames, confirmed_frames);

	// convert error value absolute
	for (i = 0; i < confirmed_frames; ++i) { error[i] = fabs(error[i]); }

//	sprintf(filename, "%ld", current_ts.tv_sec);

	if(debug_mode == VERBOSE_MODE)
	{
		ofile = stdout;
	}
	else if(debug_mode == DUMP_TO_FILE_MODE)
	{
		ofile = fopen(debug_filename, "a");
	}

	if(ofile != NULL)
	{
		fprintf(ofile, "L %.9f %.9f %.9f %.9f\n",
				MSEC*minFunc(latency, confirmed_frames), MSEC*maxFunc(latency, confirmed_frames),
				MSEC*avgFunc(latency, confirmed_frames), MSEC*stdFunc(latency, confirmed_frames));
		fprintf(ofile, "E %.9f %.9f %.9f %.9f\n",
				MSEC*minFunc(error, confirmed_frames), MSEC*maxFunc(error, confirmed_frames),
				MSEC*avgFunc(error, confirmed_frames), MSEC*stdFunc(error, confirmed_frames));

		fclose(ofile);
	}
}

/* signal handler */
void sig_handler()
{
	printNetStat();
	free(sync_stat);
	free(frame_mark);
	puts("\nProcess terminated !!\n");
}
/* Print signal information */
void printSignal(Signal *signal)
{
	double T1, T2, T3, T4;
	T1 = ts_to_sec(&signal->sender_T1);
	T2 = ts_to_sec(&signal->receiver_T2);
	T3 = ts_to_sec(&signal->receiver_T3);
	T4 = ts_to_sec(&signal->sender_T4);

	printf("code: %d name: %s seq: %u timestamp (T1, T2, T3, T4): %.9f, %.9f, %.9f %.9f message: %s\n",
			signal->code, signal->name, signal->seq, T1, T2, T3, T4, signal->message);
}


/* Print Ethernet Header */
void printEtherHeader(const void *buffer)
{
	struct ethhdr *ptr  = (struct ethhdr *)buffer;
	int i;

	//print destination field
	printf("destination mac: ");
	for(i = 0; i < ETH_ALEN; ++i)
	{
		printf("%x:", ptr->h_dest[i]);
	}printf("\t");

	//print source field
	printf("source mac: ");
	for(i = 0; i < ETH_ALEN; ++i)
	{
		printf("%x:", ptr->h_source[i]);
	}printf("\t");

	//print protocol
	printf("protocol: %x\n", ptr->h_proto);
	fflush(stdout);
}

/* Prepare socket address */
void prepare_socket_add(struct sockaddr_ll *socket_address, int ifindex, struct ifreq *ifr, unsigned char *src_mac)
{
	int i;

	/*prepare sockaddr_ll for RAW communication */
	socket_address->sll_family   = PF_PACKET;
	socket_address->sll_protocol = htons(ETH_P_IP);
	socket_address->sll_ifindex  = ifindex;				// Index of ethernet device
	socket_address->sll_hatype   = ARPHRD_ETHER;		// ARP hardware identifier is ethernet
	socket_address->sll_pkttype  = PACKET_OTHERHOST;	// target is another host
	socket_address->sll_halen    = ETH_ALEN;			// address length
	/*MAC - end*/
	socket_address->sll_addr[6]  = 0x00;/*not used*/
	socket_address->sll_addr[7]  = 0x00;/*not used*/

	for (i = 0; i < ETH_ALEN; i++)
	{
		socket_address->sll_addr[i] = ifr->ifr_hwaddr.sa_data[i];
		src_mac[i] = ifr->ifr_hwaddr.sa_data[i];
	}

	/*MAC - end*/
	socket_address->sll_addr[6]  = 0x00;/*not used*/
	socket_address->sll_addr[7]  = 0x00;/*not used*/

}

/* prepare a socket */
void prepare_socket()
{
	int ifindex = 0;
	struct ifreq ifr;

	// create raw socket
	s = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (s == -1) { printf("send_frame(): error creating socket.\n"); }

	/*retrieve ethernet interface index*/
	strncpy(ifr.ifr_name, device, IFNAMSIZ);
	if (ioctl(s, SIOCGIFINDEX, &ifr) == -1) {
		perror("send_frame(): SIOCGIFINDEX");
		usage();
		exit(EXIT_FAILURE);
	}
	ifindex = ifr.ifr_ifindex;
	printf("%s interface index: %i\n", device, ifindex);

	/* retrieve corresponding MAC */
	strncpy(ifr.ifr_name, device, IFNAMSIZ);
	if (ioctl(s, SIOCGIFHWADDR, &ifr) == -1) {
		perror("send_frame(): SIOCGIFINDEX");
		exit(1);
	}

	/* Prepare socket address */
	prepare_socket_add(&socket_address, ifindex, &ifr, src_mac);
}

/* copy timespec */
inline void copy_timespec(struct timespec *destination, struct timespec *source)
{
	destination->tv_nsec = source->tv_nsec;
	destination->tv_sec = source->tv_sec;
}

/* Flush timestamp  to 0*/
inline void flush_timestamp(SyncStat *sync)
{
	sync->sender_T1.tv_sec = 0;sync->sender_T1.tv_nsec = 0;
	sync->sender_T4.tv_sec = 0;sync->sender_T4.tv_nsec = 0;
	sync->receiver_T2.tv_sec = 0;sync->receiver_T2.tv_nsec = 0;
	sync->receiver_T3.tv_sec = 0;sync->receiver_T3.tv_nsec = 0;
	sync->remark = 0;// frame not processed yet for statistics purpose only.
}

/* switch mac addresses */
inline void swap_mac(unsigned char *mac1, unsigned char *mac2)
{
	unsigned char tmp;
	int i;
	for (i = 0; i < ETH_ALEN; ++i)
	{
		tmp = mac1[i]; mac1[i] = mac2[i]; mac2[i] = tmp;
	}
}

/* Send frame */
void *send_frame(void *ptr)
{
	puts("\nThread: sender started ...\n");

	void* buffer = (void*)malloc(frame_size);	// buffer for Ethernet frame
	unsigned char* etherhead = buffer;				// pointer to Ethernet header
	struct ethhdr *eh = (struct ethhdr *)etherhead;	// pointer to Ethernet header
	Signal *send_signal = (Signal *)(buffer + ETH_HLEN);
	int send_result = 0;

	/* construct the frame header*/
	memcpy((void*)buffer, (void*)mac, ETH_ALEN);
	memcpy((void*)(buffer+ETH_ALEN), (void*)src_mac, ETH_ALEN);
	eh->h_proto = 0x00;

	/* construct a signal */
	send_signal->code = 0x00;
	send_signal->seq = 0;
	strcpy(send_signal->name, "hit");
	strcpy(send_signal->message, "HIT ...");

	/* Send the raw Ethernet frame */
	while (1)
	{
		// flush signal timestamps
		flush_timestamp(&sync_stat[sent_frames]);

		/* update sync table*/
		if( clock_gettime( CLOCK_REALTIME, &sync_stat[sent_frames].sender_T1) == -1 )
		{
			 perror( "send_frame(): clock gettime" );
			 continue;
		}

		sem_wait(&sem);
		/*send the frame*/
		send_signal->seq = sent_frames;
		send_result = sendto(s, buffer, ETH_FRAME_LEN, 0,
				  (struct sockaddr*)&socket_address, sizeof(socket_address));

		if (send_result == -1)
		{
			printf("sender: error sending raw packet.\n");
			sem_post(&sem);
			continue;
		}

		//printEtherHeader(buffer);
		if (debug_enable) printSignal(buffer + ETH_HLEN);

		++sync_stat[sent_frames].remark; // frame sent from the client but not received back from the server yet. (=1)
		++sent_frames;
		sem_post(&sem);
		if (sent_frames == number_of_frames)
		{
			printf("maximum frames sent reached: %d\n", sent_frames);
			break;
			//printNetStat();
			//exit(EXIT_SUCCESS);
		}
		// Interval between frames in microsecond
		usleep(interval);
	}
}

/* receive frame */
void *receive_frame(void *ptr)
{
	puts("Thread: receive_frame started ...\n");

	/* receiving frame related variables */
	void* buffer = (void*)malloc(frame_size); 		// buffer for the frame to be received
	int length = 0; 									// length of the received frame
	struct ethhdr* eth_hdr = (struct ethhdr *)buffer;
	Signal *received_signal = (Signal *)(buffer + ETH_HLEN);	// pointer to the signal on the received 'buffer'
	struct timespec current;
	struct timespec last_time_frame_received;
	int notmarked = -1;

	// check timeout: if the last time frame received is beyond 1 second exit
	if( clock_gettime( CLOCK_MONOTONIC, &last_time_frame_received) == -1 ) {
	  perror( "receive_frame(): clock_gettime for current timeout" );
	}

	while(1)
	{
		length = recvfrom(s, buffer, ETH_FRAME_LEN, 0, NULL, NULL);
		if (length == -1)
		{
			printf("receive_frame(): error receiving raw packet.\n");
			break;//continue;
		}

		// check timeout: if the last time frame received is beyond 1 second exit
		if( clock_gettime( CLOCK_MONOTONIC, &current) == -1 ) {
		  perror( "receive_frame(): clock_gettime for current timeout" );
		}
		//printf("%ld %ld\n", current.tv_sec, last_time_frame_received.tv_sec);

		// check for frame receive timeout
		if (current.tv_sec > last_time_frame_received.tv_sec + 1)
		{
			printf("receive_frame(): timeout\n");
			break;
		}

		// process and validate the received frame belongs to previously sent frame and also avoid duplicate reading of a frame
		if (received_signal->code == 0x01  &&  eth_hdr->h_proto == 0)
		{
			if (received_signal->seq < number_of_frames)
			{
				if(frame_mark[received_signal->seq])
					continue;
			}frame_mark[received_signal->seq] = 1;// flag buffer read

			if(clock_gettime( CLOCK_MONOTONIC, &last_time_frame_received) == -1 )
			{
			  perror( "receive_frame(): clock_gettime for last_time_frame_received timeout" );
			}

			// Get the timestamp the frame is received and store it on sync_stat table
			if( clock_gettime( CLOCK_REALTIME, &sync_stat[received_signal->seq].sender_T4) == -1 ) {
			  perror( "receive_frame(): clock gettime" );
			  continue;
			}
			//printEtherHeader(buffer); // print the ethernet header
			if (debug_enable) printSignal(received_signal); // print the signal info

			//frame_mark[received_signal->seq] = 0; // mark the frame as read
			sem_wait(&sem);
			++sync_stat[received_signal->seq].remark; // frame received as confirmation to the send_frame() (=2)
			sem_post(&sem);

			// store the timestamp from the server side on the sync_stat table
			copy_timespec(&sync_stat[received_signal->seq].receiver_T2, &received_signal->receiver_T2); // record send_T2 timestamp
			copy_timespec(&sync_stat[received_signal->seq].receiver_T3, &received_signal->receiver_T3); // record send_T3 timestamp

			// calculate latency and error/offset the clocks between the client and the server.
			calc_sync(&sync_stat[received_signal->seq]);

			++received_frames;
		}
		else
		{
			continue;
		}

	}

}

/* Bounce frame: exchange the source and destination of the received frame and send it back to the source */
void bounce_frame(void)
{
	puts("server mode: bounce ... started\n");

	void* buffer = (void*)malloc(frame_size); /*Buffer for ethernet frame to receive*/
	struct ethhdr* eth_hdr = (struct ethhdr *)buffer;			// cast the header;
	Signal *received_signal = (Signal *)(buffer + ETH_HLEN);	// cast the signal;
	int length = 0; 											// length of the received frame*/
	int i;
	int notmarked = -1;
	int send_result = 0;

	if (signal(SIGTERM, sig_handler) == SIG_ERR)
		puts("signal could not be handled.\n");


	while(1)
	{
		length = recvfrom(s, buffer, ETH_FRAME_LEN, 0, NULL, NULL);
		if (length == -1)
		{
			printf("receiver: error receiving raw packet.\n");
			continue;
		}

		// update the received signal timestamp
		if( clock_gettime( CLOCK_REALTIME, &(received_signal->receiver_T2)) == -1 ) {
		  perror( "clock gettime" );
		  exit(1);
		}

		/* filter the frame */
		if (received_signal->code == 0  &&  eth_hdr->h_proto == 0)
		{

			//printEtherHeader(buffer); // print the ethernet header
			//printSignal(received_signal); // print the signal info

			// To bounce the frame swap the source and destination addresses
			swap_mac((unsigned char *)buffer, (unsigned char *)buffer + ETH_ALEN);

			// modify the frame code to 0x01 to show it is a bounced framed or response
			received_signal->code = 0x01;
			strcpy(received_signal->name, "bounce");
			strcpy(received_signal->message, "BOUNCED ...");

			// update the signal timestamp before sending the frame
			if( clock_gettime( CLOCK_REALTIME, &(received_signal->receiver_T3)) == -1 ) {	// update timestamp of the signal
				 perror( "clock gettime !!" );
				 exit(1);
			}

			/*send the frame */
			send_result = sendto(s, buffer, ETH_FRAME_LEN, 0,
					  (struct sockaddr*)&socket_address, sizeof(socket_address));

			if (send_result == -1)
			{
				printf("sender: error sending raw packet.\n");
			}
			//printEtherHeader(buffer);
			//printSignal(received_signal);
		}
		else
		{
			continue;
		}

	}
}

/* Bounce frame: exchange the source and destination of the received frame and send it back to the source */
void trade_frame(void)
{
	puts("server mode: trade ... started\n");

	/* receiving frame related variables */
	void* received_buffer = (void*)malloc(frame_size); /*Buffer for ethernet frame to receive*/
	int length = 0; /*length of the received frame*/
	int notmarked = -1;

	/*pointer to ethenet header*/
	struct ethhdr* eth_hdr = NULL;
	Signal *received_signal;

	/* sending frame related variables */
	void* send_buffer = (void*)malloc(ETH_FRAME_LEN); /*Buffer for ethernet frame to be sent*/
	int send_result = 0;

	if (signal(SIGTERM, sig_handler) == SIG_ERR)
		puts("signal could not be handled.\n");

	/* Create new frame/packet */
	struct ethhdr *eh = (struct ethhdr *)send_buffer;	// pointer to ethernet header
	Signal *send_signal = (Signal *)(send_buffer + ETH_HLEN);	// pointer to the signal/payload
	memcpy((void *)send_buffer, (void *)mac, ETH_ALEN);		// update destination mac
	memcpy((void *)(send_buffer+ETH_ALEN), (void *)src_mac, ETH_ALEN);	// update source mac
	eh->h_proto = 0x00;
	// Fill up the signal attributes
	send_signal->code = 0x01;
	strcpy(send_signal->message, "TRADE ...");
	send_signal->seq = 0;
	strcpy(send_signal->name, "trade");

	/* receive frames */
	while(1)
	{
		length = recvfrom(s, received_buffer, ETH_FRAME_LEN, 0, NULL, NULL);
		if (length == -1)
		{
			printf("receiver: error receiving raw packet.\n");
			continue;
		}
		eth_hdr = (struct ethhdr *)received_buffer;					// cast the header
		received_signal = (Signal *)(received_buffer + ETH_HLEN);	// cast the signal

		//printf("ethr_hdr = %p, latency_signal = %p\n", eth_hdr, received_signal);

		/* filter the frame */
		if (received_signal->code == 0x0  &&  eth_hdr->h_proto == 0 && notmarked != received_signal->seq)
		{
			notmarked = received_signal->seq;
			//frame_mark[received_signal->seq] = 0; // mark the frame as read
			//printEtherHeader(received_buffer);
			if (debug_enable) printSignal(received_signal);

			// update the signal timestamp before sending the frame
			if( clock_gettime( CLOCK_REALTIME, &send_signal->receiver_T2) == -1 ) {	// update timestamp of the signal
				 perror( "trade_frame(): clock gettime !!" );
				 exit(1);
			}

			send_signal->seq = received_signal->seq;
			/*send the frame*/
			send_result = sendto(s, send_buffer, ETH_FRAME_LEN, 0,
					  (struct sockaddr*)&socket_address, sizeof(socket_address));

			if (send_result == -1)
			{
				printf("sender: error sending raw packet.\n");
			}
			//printEtherHeader(send_buffer);
			if (debug_enable) printSignal(send_signal);

		}
		else
		{
			continue;
		}

	}
}

/* start of application */
int main(int argc, char **argv)
{
	pthread_t sender_thread, receiver_thread;
    int  iret_sender, iret_receiver, i;
    Argument cmdline_arg;
    struct sched_param sender_param, receiver_param, client_param;
    sem_init(&sem, 0, 1);

    /* process command line */
    proces_cmdline(argc, argv);

    /* verfiy command line */
    //@TODO: verify_commandline();

    /* Initialize variables */
    /* workaround: mark frames received fromt the server to avoid receiving them again */
    frame_mark = malloc(sizeof(int)*number_of_frames);
    if (frame_mark == NULL)
    {
    	printf("main(): error initializing frame_mark.\n");
    }

    sync_stat = malloc(sizeof(SyncStat)*number_of_frames);
    if (frame_mark == NULL)
	{
    	 printf("main(): error initializing sync_stat.\n");
	}for (i = 0; i < number_of_frames; ++i) { frame_mark[i] = 0; }

	// Create a signal handler if the program is terminated
	if (signal(SIGTERM, sig_handler) == SIG_ERR)
		puts("signal could not be handled.\n");

    /* prepare socket with Ethernet protocol*/
    prepare_socket();

    if (mode == CLIENT_MODE)
    {
    	puts("mode: client mode.");

    	/* Create sender thread */
		iret_sender = pthread_create( &sender_thread, NULL, send_frame, (void*) &cmdline_arg);
		// make the thread real-time task with higher priority of 98 for example.
		sender_param.__sched_priority = 98;
		if ((i = pthread_setschedparam(sender_thread, SCHED_FIFO, &sender_param)) != 0)
		{
			handle_error_en(s, "error: sender_pthread_setschedparam");
		}

		/* Create receiver thread*/
		iret_receiver = pthread_create( &receiver_thread, NULL, receive_frame, (void*) &cmdline_arg);
		// make the thread real-time task with higher priority of 98 for example.
		receiver_param.__sched_priority = 98;
		if ((i =pthread_setschedparam(receiver_thread, SCHED_FIFO, &receiver_param)) != 0)
		{
			handle_error_en(s, "error: receiver_pthread_setschedparam");
		}

		/* Wait till threads are complete before main continues. Unless we  */
		/* wait we run the risk of executing an exit which will terminate   */
		/* the process and all threads before the threads have completed.   */


		pthread_join( sender_thread, NULL);
		pthread_join( receiver_thread, NULL);

		printNetStat();

		printf("sender_thread 1 returns: %d\n",iret_sender);
		printf("receiver_thread 2 returns: %d\n",iret_receiver);
    }
    else if(mode == SERVER_MODE)
    {
    	client_param.__sched_priority = 98;
    	sched_setscheduler(0, SCHED_FIFO, &client_param);
    	printf("mode: %d server mode.\n", server_mode);
    	if (server_mode == SERVER_BOUNCE_MODE)
    	{
    		bounce_frame();
    	}
    	else if(server_mode == SERVER_TRADE_MODE)
    	{
    		trade_frame();
    	}else
    		puts("Error: server mode unknown.");

    }

    exit(0);
}
